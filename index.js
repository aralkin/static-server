const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');
const { promisify } = require('util');
const readFilePromisify = promisify(fs.readFile);
const statPromisify = promisify(fs.stat);
const readDirPromisify = promisify(fs.readdir);

const createPage = (fileList) =>
  `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
      a {
        text-decoration: none;
        color: blue;
      }
      .directory-list {
        list-style: none;
      }
      .directory-item {
        margin-bottom: 10px;
      }
    </style>
</head>
<body>
    <ul class="directory-list">
        ${fileList.map(
            ({path, fileName}) => `<li class="directory-item"><a href="${path}">${fileName}</a></li>`
        ).join('')}
    </ul>
</body>
</html>`;

http
    .createServer(async (req, res) => {
        const parsedUrl = url.parse(req.url);
        const filterPathExit = path.normalize(parsedUrl.pathname).replace(/^(\.\.[\/\\])+/, '');
        const pathname = path.join(__dirname, filterPathExit);

        try {
            if ((await statPromisify(pathname)).isDirectory()) {
                const files = await readDirPromisify(pathname);
                const rootPath = filterPathExit === '\\' ? '/' : filterPathExit + '/';
                const dirs = files.map(fileName => ({ path: rootPath + fileName, fileName }));
                const renderedList = createPage(dirs);
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.end(renderedList);
                return;
            }
        } catch(e) {
            res.statusCode = 404;
            res.end(`File ${pathname} not found!`);
            return;
        }

        try {
            const data = await readFilePromisify(pathname);
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end(data);
        } catch (err) {
            res.statusCode = 500;
            res.end(`Error getting the file: ${err}.`);
        }
    })
    .listen(3000, () => {
        console.log('Server is running on localhost:3000/');
    });